var mongoose = require('mongoose');
var Schema    = mongoose.Schema;

var contactSchema = new Schema ({
    primarycontactnumber: {
        type: String, index: {
            unique:
            true
        }
    },
    firstname: String,
    lastname: String,
    title: String,
    company: String,
    jobtitle: String,
    othercontactnumbers: [String],
    Heprimaryemailaddress: String,
    emailaddresses: [String],
    groups: [String]
});

module.exports = mongoose.model('Contact', contactSchema);