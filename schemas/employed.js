var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var possibleTypesDoc = ['cc', 'ce', 'pasaporte'];

var employed = new Schema({
    numDoc: { type: String, index: { unique: true } },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    typeDocument: { type: String, enum: possibleTypesDoc , required : true},
    position: String
});

module.exports = mongoose.model('employed', employed);