var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var request = new Schema({
    dateRequest: {
        type: Number
    },
    ownerId: {
        type: String
    },
    financialId: {
        type: String,
        required: true
    },
    employedId: {
        type: String,
        required: true
    },
    companyId: {
        type: String,
        required: true
    },
    ammount: {
        type: Number,
        required: true
    },
    price: {
        type: Number
    },
    salesvalue: {
        type: Number
    },
    validatedByCompany: {
        type: Boolean,
        default: false
    },
    validatedByFinancial: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model('request', request);
