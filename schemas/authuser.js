var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var possibleRoles = ['admin', 'employed', 'financial','company'];

var authSchema = new Schema({
    username: { type: String, index: { unique: true } },
    password: String,
    role: {type : String, enum :possibleRoles },
});

module.exports = mongoose.model('User', authSchema);