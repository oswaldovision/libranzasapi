var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var accountCredit = new Schema({
    employed: {
        type: String,
        required: true
    },
    financial: {
        type: String,
        required: true
    },
    amount: {
        type: Number,
        required: true
    },
    dateRequest: {
        type: Number,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    salesValue: {
        type: Number,
        required: true
    },
    address: {
        type: String,
        index: {
            unique: true
        }
    }

});

module.exports = mongoose.model('accountCredit', accountCredit);
