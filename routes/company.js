var express = require('express');
var url = require("url");
var router = express.Router();

var repository = require('../modules/repository');
var schema = require('../schemas/company');

//constant
var keySchema = 'nit';
var roleAdmin = 'admin';

router.get('/:nit', function (req, res) {
    repository.query_by_arg(schema, keySchema, req.params.nit, res);
});

router.get('/', function (req, res) {
    var get_params = url.parse(req.url, true).query;
    if (Object.keys(get_params).length == 0) {
        repository.list(schema, res);
    } else {
        var key = Object.keys(get_params)[0];
        var value = get_params[key];
        JSON.stringify(repository.query_by_arg(schema, key, value, res));
    }
});

router.post('/', function (req, res) {
    var newCompany = toCompany(req.body);

    var filterArg = '{"' + keySchema + '":' + '"' + newCompany.nit + '"}';
    var filter = JSON.parse(filterArg);

    schema.findOne(filter, function (error, data) {
        if (error) {
            res.writeHead(500, { 'Content-Type': 'text/plain' });
            res.end(error);
        }
        if (data) {
            data.nit = newCompany.nit;
            data.name = newCompany.name;
            data.address = newCompany.address;
            data.phones = newCompany.phones;
            data.save();
            res.writeHead(200, { 'Content-Type': 'text/plain' });
            res.end("updated !");
            return;
        }

        res.writeHead(500, { 'Content-Type': 'text/plain' });
        res.end("it does not exist !");
    })

});

router.put('/', function (req, res) {
    var company = toCompany(req.body);
    repository.create(company, res);
});

router.delete('/:nit', function (req, res) {
    if (authorize(req.user, res)) {
        repository.remove(schema, keySchema, req.params.nit, res);
    } else {
        res.writeHead(403, { 'Content-Type': 'text/plain' });
        res.end('Forbidden')
        return;
    }
});

function toCompany(body) {
    return new schema(
        {
            nit: body.nit,
            name: body.name,
            phones: body.phones,
            address: body.address
        });
}

function authorize(user) {
    if ((user == null) || (user.role != roleAdmin)) {
        return false;
    }
    return true;
}

module.exports = router;