var express = require('express');
var url = require("url");
var router = express.Router();
var Web3 = require("web3");
var async = require("asyncawait/async");
var await = require("asyncawait/await");

var web3, account;
var contract = require('truffle-contract');
var credit_artifacts = require('../build/contracts/AccountCredit.json');
var Credit = contract(credit_artifacts);

var repository = require('../modules/repository');
var schema = require('../schemas/accountCredit');

const keySchema = 'address';

router.get('/:address', function (req, res) {

    if (typeof web3 == 'undefined') setProvider();

    var credit = Credit.at(req.params.address);
    var objCredit = {};

    var buildResult = async(function () {
        objCredit.employed = await (credit.employed.call());
        objCredit.financial = await (credit.financial.call());
        objCredit.amount = await (credit.amount.call());
        objCredit.dateRequest = await (credit.dateRequest.call() || 0);
        objCredit.price = await (credit.price.call() || 0);
        objCredit.salesValue = await (credit.salesValue.call() || 0);
        return objCredit;
    });

    buildResult().then(function (obj) {
        res.setHeader('Content-Type', 'application/json');
        res.send(obj);
    }).catch(function (err) {
        res.writeHead(500, {
            'Content-Type': 'text/plain'
        });
        res.end("somethin is wrong: " + err);
    });
});

router.get('/', function (req, res) {
    var get_params = url.parse(req.url, true).query;
    if (Object.keys(get_params).length == 0) {
        repository.list(schema, res);
    } else {
        var key = Object.keys(get_params)[0];
        var value = get_params[key];
        JSON.stringify(repository.query_by_arg(schema, key, value, res));
    }
});

router.post('/changeFinancial', function (req, res) {
    if (typeof web3 == 'undefined') setProvider();
    
    console.log(req.body);
    
    var credit = Credit.at(req.body.address);

    var changeFinancial = async(function (financial) {
        return await (credit.changeFinancial(financial, {
            account,
            from: account,
            gas: 1000000
        }));
    })

    var changeSalesValue = async(function (salesValue) {
        return await (credit.changeSalesValue(salesValue, {
            account,
            from: account,
            gas: 1000000
        }));
    })

    changeFinancial(req.body.financial).then(function (codeTranChangeFinancial) {

        changeSalesValue(req.body.salesValue).then(function (codeTranSalesValue) {
            //create accountCredir in BD
            var accountCredit = toAccountCredit(req.body, null);

            var filterArg = '{"' + keySchema + '":' + '"' + req.body.address + '"}';
            var filter = JSON.parse(filterArg);

            schema.findOne(filter, function (error, data) {
                if (error) {
                    res.writeHead(500, {
                        'Content-Type': 'text/plain'
                    });
                    res.end(error);
                }
                if (data) {
                    data.financial = accountCredit.financial;
                    data.salesValue = accountCredit.salesValue;
                    data.save();

                    res.setHeader('Content-Type', 'application/json');
                    res.send({ codeTranChangeFinancial, codeTranSalesValue});
                    return;
                }
                res.writeHead(500, { 'Content-Type': 'text/plain' });
                res.end("it does not exist !");
            });
        });
    }).catch(function (err) {
        res.writeHead(500, {
            'Content-Type': 'text/plain'
        });
        res.end("somethin is wrong: " + err);
    });
})

router.post('/changePrice',function(req,res){
    if (typeof web3 == 'undefined') setProvider();
    
    var credit = Credit.at(req.body.address);

    var changePrice = async(function (price) {
        return await (credit.changePrice(price, {
            from: account
        }));
    })
    
    changePrice(req.body.price).then(function(codeTransaction){
        //create accountCredir in BD
        var accountCredit = toAccountCredit(req.body, null);
        
        var filterArg = '{"' + keySchema + '":' + '"' + req.body.address + '"}';
        var filter = JSON.parse(filterArg);

        schema.findOne(filter, function (error, data) {
            if (error) {
                res.writeHead(500, {
                    'Content-Type': 'text/plain'
                });
                res.end(error);
            }
            if (data) {
                data.price = accountCredit.price;
                data.save();

                res.setHeader('Content-Type', 'application/json');
                res.send(codeTransaction);
                return;
            }
            res.writeHead(500, { 'Content-Type': 'text/plain' });
            res.end("it does not exist !");
        });
    })
    
})

router.post('/changeSalesValue',function(req,res){
    if (typeof web3 == 'undefined') setProvider();
    
    var credit = Credit.at(req.body.address);

    var changeSalesValue = async(function (salesValue) {
        return await (credit.changePrice(salesValue, {
            from: account
        }));
    })
    
    changeSalesValue(req.body.salesValue).then(function(codeTransaction){
        //create accountCredir in BD
        var accountCredit = toAccountCredit(req.body, null);
        
        var filterArg = '{"' + keySchema + '":' + '"' + req.body.address + '"}';
        var filter = JSON.parse(filterArg);

        schema.findOne(filter, function (error, data) {
            if (error) {
                res.writeHead(500, {
                    'Content-Type': 'text/plain'
                });
                res.end(error);
            }
            if (data) {
                data.salesValue = accountCredit.salesValue;
                data.save();

                res.setHeader('Content-Type', 'application/json');
                res.send(codeTransaction);
                return;
            }
            res.writeHead(500, { 'Content-Type': 'text/plain' });
            res.end("it does not exist !");
        });
    })
    
})

router.put('/', function (req, res) {
    console.log(web3 + "   :este es web3");

    if (typeof web3 == 'undefined') setProvider();

    console.log(JSON.stringify(req.body));
    console.log(account);
    Credit.new(req.body.employed, req.body.financial, req.body.amount, req.body.dateRequest, req.body.price, req.body.salesValue, {
        account,
        from: account,
        gas: 1000000
    }).then(function (instance) {
        console.log('Creado Credito en blockchain...');

        //create accountCredir in BD
        var accountCredit = toAccountCredit(req.body, instance.address);

        var saveAccountCredit = async(function (credit) {
            await (credit.save());
        });

        saveAccountCredit(accountCredit);

        res.writeHead(201, {
            'Content-Type': 'text/plain'
        });
        res.end("Se ha creado con exito el pagare: " + instance.address);

    }).catch(function (err) {
        console.log('ERROR credito en blockchain...' + err);
        res.writeHead(500, {
            'Content-Type': 'text/plain'
        });
        res.end('Err !' + err);
    })

});

var setProvider = function () {
    console.log('build instance web3...');
    web3 = new Web3(new Web3.providers.HttpProvider("http://127.0.0.1:8095"));

    if (web3.isConnected()) {
        console.log('Conectado...');
        web3.eth.defaultAccount = web3.eth.accounts[0];
        account = web3.eth.accounts[0];
    }

    Credit.setProvider(web3.currentProvider);
}

var toAccountCredit = function (body, addressCredit) {
    return new schema({
        employed: body.employed,
        financial: body.financial,
        amount: body.amount,
        dateRequest: body.dateRequest,
        price: body.price || 0,
        salesValue: body.salesValue || 0,
        address: addressCredit || body.address
    });
}

module.exports = router;
