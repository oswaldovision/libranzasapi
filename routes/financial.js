var express = require('express');
var url = require("url");
var router = express.Router();

var repository = require('../modules/repository');
var schema = require('../schemas/financial');

//constant
var keySchema = 'nit';
var roleAdmin = 'admin';

router.get('/:nit', function (req, res) {
    repository.query_by_arg(schema, keySchema, req.params.nit, res);
});

router.get('/', function (req, res) {
    var get_params = url.parse(req.url, true).query;
    if (Object.keys(get_params).length == 0) {
        repository.list(schema, res);
    } else {
        var key = Object.keys(get_params)[0];
        var value = get_params[key];
        JSON.stringify(repository.query_by_arg(schema, key, value, res));
    }
});

router.put('/', function (req, res) {
    var financial = toFinancial(req.body);
    repository.create(financial, res);
});

router.post('/', function (req, res) {
    var newFinancial = toFinancial(req.body);

    var filterArg = '{"' + keySchema + '":' + '"' + newFinancial.nit + '"}';
    var filter = JSON.parse(filterArg);

    schema.findOne(filter, function (error, data) {
        if (error) {
            res.writeHead(500, { 'Content-Type': 'text/plain' });
            res.end(error);
        }
        if (data) {
            data.nit = newFinancial.nit;
            data.name = newFinancial.name;
            data.address = newFinancial.address;
            data.phones = newFinancial.phones;
            data.save();
            res.writeHead(200, { 'Content-Type': 'text/plain' });
            res.end("updated !");
            return;
        }

        res.writeHead(500, { 'Content-Type': 'text/plain' });
        res.end("it does not exist !");
    })

});

router.delete('/:nit', function (req, res) {
    if (authorize(req.user, res)) {
        repository.remove(schema, keySchema, req.params.nit, res);
    } else {
        res.writeHead(403, { 'Content-Type': 'text/plain' });
        res.end('Forbidden')
        return;
    }
});

function toFinancial(body) {
    return new schema(
        {
            nit: body.nit,
            name: body.name,
            phones: body.phones,
            address: body.address
        });
}

function authorize(user) {
    if ((user == null) || (user.role != roleAdmin)) {
        return false;
    }
    return true;
}

module.exports = router;
