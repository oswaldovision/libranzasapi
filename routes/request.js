var express = require('express');
var url = require("url");
var moment = require("moment");
var router = express.Router();

var repository = require('../modules/repository');
var schema = require('../schemas/request');

//constant
var keySchema = '_id';
var roleAdmin = 'admin';

router.get('/:id', function (req, res) {
    repository.query_by_arg(schema, keySchema, req.params.id, res);
});

router.get('/', function (req, res) {
    var get_params = url.parse(req.url, true).query;
    if (Object.keys(get_params).length == 0) {
        repository.list(schema, res);
    } else {
        var key = Object.keys(get_params)[0];
        var value = get_params[key];
        JSON.stringify(repository.query_by_arg(schema, key, value, res));
    }
});

router.post('/', function (req, res) {
    var newRequest = toRequest(req.body);

    var filterArg = '{"' + keySchema + '":' + '"' +req.body._id + '"}';
    var filter = JSON.parse(filterArg);

    schema.findOne(filter, function (error, data) {
        if (error) {
            res.writeHead(500, { 'Content-Type': 'text/plain' });
            res.end(error);
        }
        if (data) {
//            data.dateRequest = newRequest.dateRequest;
            data.ownerId = newRequest.ownerId;
            data.financialId = newRequest.financialId;
            data.employedId = newRequest.employedId;
            data.companyId = newRequest.companyId;
            data.ammount = newRequest.ammount;
            data.price = newRequest.price;
            data.salesvalue = newRequest.salesvalue;
            data.validatedByCompany = newRequest.validatedByCompany;
            data.validatedByFinancial = newRequest.validatedByFinancial;                
            
            data.save();
            res.writeHead(200, { 'Content-Type': 'text/plain' });
            res.end("updated !");
            return;
        }

        res.writeHead(500, { 'Content-Type': 'text/plain' });
        res.end("it does not exist !");
    })

});

router.put('/', function (req, res) {
    var financial = toRequest(req.body);
    repository.create(financial, res);
});

router.delete('/:id', function (req, res) {
    if (authorize(req.user, res)) {
        repository.remove(schema, keySchema, req.params.id, res);
    } else {
        res.writeHead(403, { 'Content-Type': 'text/plain' });
        res.end('Forbidden')
        return;
    }
});

function toRequest(body) {
    return new schema(
        {
            dateRequest: moment().valueOf(),
            ownerId: body.ownerId,
            financialId: body.financialId,
            employedId: body.employedId,
            companyId: body.companyId,
            ammount: body.ammount,
            price: body.price,
            salesvalue: body.salesvalue,
            validatedByCompany : body.validatedByCompany,
            validatedByFinancial : body.validatedByFinancial,
        });
}

function authorize(user) {
    if ((user == null) || (user.role != roleAdmin)) {
        return false;
    }
    return true;
}

module.exports = router;
