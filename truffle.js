// Allows us to use ES6 in our migrations and tests.
require('babel-register');

module.exports = {
    networks: {
        development: {
            host: "localhost",
            port: 8545,
            network_id: "*" // Match any network id
        },
        private: {
            host: "127.0.0.1",
            port: 8095,
            network_id: 13874025, // Match any network id
            gas: 4712388
        }
    }
};
