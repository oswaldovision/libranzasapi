pragma solidity ^0.4.4;

contract AccountCredit{
    
    address owner;
    
    string public employed;
    string public financial;
    uint public amount;
    uint public dateRequest;
    uint public price;
    uint public salesValue;

    
    function AccountCredit(string _employed,string _financial,uint _amount,uint _dateRequest, uint _price, uint _salesValue){
            employed = _employed;
            financial = _financial;
            amount = _amount;
            dateRequest = _dateRequest;  
            salesValue = _salesValue;
            price = _price;
    }    
    
    function changeFinancial(string newFinancial){
        financial = newFinancial;
    }
    
    function changePrice(uint newPrice) returns(uint){
        price = newPrice;
        return price;
    }
    
    function changeSalesValue(uint newValue){
        salesValue = newValue;
    }
    
    function remove() {
        if (msg.sender == owner){
            selfdestruct(owner);
        }
    }
}